package com.nn.constents;


/**
 * 常量类
 * @author 年年 
 * 
 * @// TODO: 2022/11/11 19:10
 */
public class SystemConstants {
    /**
     * 文章是草稿
     */
    public static final int ARTICLE_STATUS_DRAFT = 1;

    /**
     * 文章正常发布
     */
    public static final int ARTICLE_STATUS_NORMAL = 0;

    /**
     * 友链正常通过
     */
    public static final int LINK_STATUS_NORMAL = 0;


    /**
     * redis存的数据
     */

    public static final String REDIS_KEY ="blogLogin";


    /**
     * 根评论id
     */
    public static final int RootID =-1;
}
