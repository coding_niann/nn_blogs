package com.nn.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 友链的响应结果类
 * @author 年年 
 * @// TODO: 2022/11/12 00:47
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LinkListVo {
    private Long id;
    private String name;
    private String logo;
    private String description;
    private String address;

    private String status;

}
