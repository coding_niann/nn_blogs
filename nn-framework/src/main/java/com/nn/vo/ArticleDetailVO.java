package com.nn.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 年年
 * 文章详细信息响应类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleDetailVO {

    private Long id;
    //标题
    private String title;
    //文章摘要
    private String summary;

    private Long categoryId;
    //所属分类名
    private String  categoryName;
    //缩略图
    private String thumbnail;
    //访问量
    private Long viewCount;

    private String content;

    private Date createTime;
}
