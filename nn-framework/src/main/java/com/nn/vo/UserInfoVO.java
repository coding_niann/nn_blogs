package com.nn.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


/**
 * @author 年年 
 * @// TODO: 2022/11/15  00:24 
 * 登录响应结果类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class UserInfoVO {
    private Long id;
    private String nickName;
    private String avatar;
    private String sex;
    private String email;
}

