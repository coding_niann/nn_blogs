package com.nn.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CategoryListAllVO {
    private String description;
    private Long id;
    private String name;

    private String  status;
}
