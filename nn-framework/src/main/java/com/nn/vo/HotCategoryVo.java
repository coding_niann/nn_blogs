package com.nn.vo;


import lombok.Data;

@Data
public class HotCategoryVo {
    private Long id;
    private String name;
}
