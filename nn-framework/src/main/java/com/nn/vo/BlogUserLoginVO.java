package com.nn.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author 年年
 * @// TODO: 2022/11/15
 * 登录响应结果类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BlogUserLoginVO {
    private String token;
    private UserInfoVO userInfo;
}
