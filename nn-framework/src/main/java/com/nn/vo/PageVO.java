package com.nn.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 年年
 * @// TODO: 2022/11/11
 *
 * 分页响应字段
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageVO {

    private List rows;
    private Long total;
}
