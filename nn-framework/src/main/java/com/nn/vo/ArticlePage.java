package com.nn.vo;


import lombok.Data;

import java.util.Date;

/**
 *
 * @author 年年
 * @// TODO: 2022/11/11 22:09
 *
 * 文章分页查询的查询结果类
 *
 */
@Data
public class ArticlePage {

    private Long id;
    //标题
    private String title;
        //文章摘要
    private String summary;

    //所属分类名
    private String  categoryName;
    //缩略图
    private String thumbnail;
    //访问量
    private Long viewCount;

    private Long categoryId;

    private Date createTime;


}
