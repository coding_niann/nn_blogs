package com.nn.vo;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *标签分页响应字段
 */
@Data
@AllArgsConstructor
public class TagPageVO {
    private Long id;

    //备注
    private String remark;

    //标签名
    private String name;


    public TagPageVO(){

    }


}
