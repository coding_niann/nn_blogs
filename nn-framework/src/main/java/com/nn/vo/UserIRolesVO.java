package com.nn.vo;


import com.nn.entity.SysRole;
import com.nn.entity.SysUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserIRolesVO {
    List<Long> roleIds;
    List<SysRole> roles;
    SysUser user;
}
