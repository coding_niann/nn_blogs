package com.nn.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 查询标签DTO
 * @author 年年
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TagListDto {

    private String name;
    private String remark;
}
