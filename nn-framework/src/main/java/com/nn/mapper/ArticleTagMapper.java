package com.nn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nn.entity.ArticleTag;

import java.util.List;


/**
 * 文章标签关联表(ArticleTag)表数据库访问层
 *
 * @author 年年
 * @since 2022-12-12 21:41:16
 */
public interface ArticleTagMapper extends BaseMapper<ArticleTag> {

    List<Long> selectTagIdList(Long id);
}

