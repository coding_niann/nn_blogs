package com.nn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nn.entity.SysRole;
import com.nn.entity.SysUser;

import java.util.List;


/**
 * 用户表(SysUser)表数据库访问层
 *
 * @author 年年
 * @since 2022-11-14 23:10:27
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 用户关联的角色id列表
     * @param id
     */
    List<Long> selectUserJoinRole(Long id);


    /**
     * 查询所有角色
     * @param
     * @return
     */
    List<SysRole> selectRoleAll();


    /**
     * 查询用户信息
     * @param id
     * @return
     */
    SysUser selectUserByID(Long id);
}

