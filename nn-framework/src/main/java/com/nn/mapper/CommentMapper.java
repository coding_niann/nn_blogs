package com.nn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nn.entity.Comment;


/**
 * 评论表(Comment)表数据库访问层
 *
 * @author 年年
 * @since 2022-11-16 00:54:32
 */
public interface CommentMapper extends BaseMapper<Comment> {

}

