package com.nn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nn.entity.Link;


/**
 * 友链(Link)表数据库访问层
 *
 * @author 年年
 * @since 2022-11-12 00:04:12
 */
public interface LinkMapper extends BaseMapper<Link> {

}

