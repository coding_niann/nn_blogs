package com.nn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nn.entity.SysRole;

import java.util.List;


/**
 * 角色信息表(SysRole)表数据库访问层
 *
 * @author 年年
 * @since 2022-12-03 23:12:56
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 根据用户id查寻角色信息
     * @param id
     * @return
     */
    List<String> selectRoleKeyByUserId(Long id);
}

