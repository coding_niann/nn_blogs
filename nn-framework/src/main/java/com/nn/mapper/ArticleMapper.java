package com.nn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nn.entity.Article;
import org.apache.ibatis.annotations.Update;

/**
 * 文章表(Article)表数据库访问层
 *
 * @author makejava
 * @since 2022-11-11 16:13:52
 */


public interface ArticleMapper extends BaseMapper<Article> {


    /**
     * 增加浏览量
     */
    @Update("update article\n" +
            "set view_count = view_count+1\n" +
            "where id=#{id};")
    void updateViewCountById(Long id);


}

