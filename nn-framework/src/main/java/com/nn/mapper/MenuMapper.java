package com.nn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nn.entity.SysMenu;
import com.nn.vo.MenusListVo;

import java.util.List;

public interface MenuMapper extends BaseMapper<SysMenu> {




    List<String> selectPermsByUserId(Long id);



    List<MenusListVo> selectAllRouterMenu();

    List<MenusListVo> selectRouterMenuById(Long id);

    /**
     * 根据roleID查询menu信息s
     * @param id
     * @return
     */
    List<SysMenu>
    selectMenuByRoleID(Long id);
}
