package com.nn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nn.entity.SysUserRole;


/**
 * 用户和角色关联表(SysUserRole)表数据库访问层
 *
 * @author 年年
 * @since 2022-12-07 21:40:03
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}

