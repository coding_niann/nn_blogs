package com.nn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nn.entity.Tag;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 标签(Tag)表数据库访问层
 *
 * @author 年年
 * @since 2022-11-18 19:11:13
 */
public interface TagMapper extends BaseMapper<Tag> {


    /**
     * 通过id批量删除标签
     * @param ids
     */
    void deleteTagByListID(@Param("ids") List<Integer> ids);
}

