package com.nn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nn.entity.Category;

/**
 * 分类表(Category)表数据库访问层
 *
 * @author 年年
 * @since 2022-11-11 20:09:24
 */
public interface CategoryMapper extends BaseMapper<Category> {


}

