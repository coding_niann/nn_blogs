package com.nn.service;


import com.nn.R;
import com.nn.entity.SysUser;

/**
 * @author 年年
 * @// TODO: 2022/11/14
 * 登录接口业务层
 */
public interface BlogLoginService {

    R login(SysUser user);

    R logout();
}
