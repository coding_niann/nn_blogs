package com.nn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nn.R;
import com.nn.entity.Comment;


/**
 * 评论表(Comment)表服务接口
 *
 * @author 年年
 * @since 2022-11-16 00:54:32
 */
public interface CommentService extends IService<Comment> {


    /**
     * 获取评论
     * @return
     */
    R getCommentListByarticleId(String type,Long articleId, int pageNum, int pageSize);



    /**
     * 添加评论
     * @param comment
     * @return
     */
    R addComment(Comment comment);
}

