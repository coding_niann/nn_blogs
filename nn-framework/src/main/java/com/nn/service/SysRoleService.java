package com.nn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nn.R;
import com.nn.entity.SysRole;

import java.util.List;


/**
 * 角色信息表(SysRole)表服务接口
 *
 * @author 年年
 * @since 2022-12-03 23:12:56
 */
public interface SysRoleService extends IService<SysRole> {

    /**
     * 根据用户id查寻角色信息
     * @param id
     * @return
     */
    List<String> selectRoleKeyByUserId(Long id);


    /**
     * 查询所有角色
     * @return
     */
    R listAll();


    /**
     * 分页
     * @param pageNum
     * @param pageSize
     * @param roleName
     * @param status
     * @return
     */
    R pageList(Integer pageNum, Integer pageSize, String roleName, String status);

    /**
     * 设置状态
     * @param role
     * @return
     */
    R changeStatus(String id,String status);

    /**
     * 删除角色
     * @param ids
     * @return
     */
    R deleteRoleByID(List<Long> ids);


    /**
     * 根据角色id获取角色信息
     * @param id
     * @return
     */
    R getRoleInfoByID(Long id);
}

