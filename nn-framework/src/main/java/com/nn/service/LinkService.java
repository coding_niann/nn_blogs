package com.nn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nn.R;
import com.nn.entity.Link;

import java.util.List;


/**
 * 友链(Link)表服务接口
 *
 * @author 年年
 * @since 2022-11-12 00:04:12
 */
public interface LinkService extends IService<Link> {

    public R getAllLink();


    /**
     * 友链管理查询
     * @param pageNum
     * @param pageSize
     * @param name
     * @param status
     * @return
     */
    R getPageList(Integer pageNum, Integer pageSize, String name, String status);


    /**
     * 增加友链
     * @param link
     * @return
     */
    R addLink(Link link);


    /**
     * 删除友链
     * @param ids
     * @return
     */
    R delectLink(List<Long> ids);


    /**
     * 根据id查询友链
     * @param id
     * @return
     */
    R getListByid(Long id);


    /**
     * 修改友链信息
     * @param link
     * @return
     */
    R updateLink(Link link);


    /**
     * 根据id修改审核状态
     * @param id
     * @param status
     * @return
     */
    R updateStatusByID(Link link);
}

