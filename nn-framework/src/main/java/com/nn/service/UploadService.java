package com.nn.service;

import com.nn.R;
import org.springframework.web.multipart.MultipartFile;

public interface UploadService {

    /**
     * 文件上传
     * @return
     */
    R uploadImg(MultipartFile img);
}
