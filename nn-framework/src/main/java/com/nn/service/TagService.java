package com.nn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nn.R;
import com.nn.domain.dto.TagListDto;
import com.nn.entity.Tag;
import com.nn.vo.PageVO;
import com.nn.vo.TagPageVO;

import java.util.List;


/**
 * 标签(Tag)表服务接口
 *
 * @author 年年
 * @since 2022-11-18 19:11:13
 */
public interface TagService extends IService<Tag> {

    /**
     *标签列表分页查询
     * @param pageNum
     * @param pageSize
     * @param tagListDto
     * @return
     */
    R<PageVO> pageTagList(Integer pageNum, Integer pageSize, TagListDto tagListDto);


    /**
     * 添加标签
     * @return
     */
    R addTag(TagListDto tagListDto);


    /**
     * 删除标签
     * @param ids
     * @return
     */
    R deleteList(List<Integer> ids);


    /**
     * 通过id查询标签信息
     * @param id
     * @return
     */
    R getTagInfo(Integer id);


    /**
     * 通过id修改标签信息
     * @param tagInfo
     * @return
     */
    R updateTag(TagPageVO tagInfo);
}

