package com.nn.service;

import com.nn.R;
import com.nn.entity.SysUser;

import java.util.List;


/**
 * @author 年年 
 * @// TODO: 2022/11/16 00:42
 */
public interface UserService {


    /**
     * 用户注册
     * @param user
     * @return
     */
    R register(SysUser user);

    /**
     * 更新个人资料
     * @param user
     * @return
     */
    R updateUserInfo(SysUser user);

    R selectUserInfo();

    /**
     * 用户信息查询
     * @param pageNum
     * @param pageSize
     * @param userName
     * @param phoneNumber
     * @param status
     * @return
     */
    R pageList(Integer pageNum, Integer pageSize, String userName, String phoneNumber, String status);

    /**
     * 设置停用启用
     * @param
     * @return
     */
    R updateStatus(Long id,String status);

    /**
     * 根据id查用户信息
     * @param id
     * @return
     */
    R selectUserInfoById(Long id);

    /**
     * 添加用户
     * @param user
     * @return
     */
    R addUser(SysUser user);


    /**
     * 删除用户
     * @return
     */
    R deleteUser(List<Long> ids);
}
