package com.nn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nn.entity.ArticleTag;

import java.util.List;


/**
 * 文章标签关联表(ArticleTag)表服务接口
 *
 * @author 年年
 * @since 2022-12-12 21:41:16
 */
public interface ArticleTagService extends IService<ArticleTag> {

    /**
     * 根据文章id查询标签列表
     * @param id
     * @return
     */
    List<Long> selectTagIdList(Long id);
}

