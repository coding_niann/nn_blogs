package com.nn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nn.R;
import com.nn.entity.Category;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 分类表(Category)表服务接口
 *
 * @author 年年
 * @since 2022-11-11 20:09:24
 */
public interface CategoryService extends IService<Category> {

    /**
     * 获取分类信息
     * @return
     */
    R getCategoryList();


    /**
     * 获取所有分类名
     * @return
     */
    R getListAllCategory();


    /**
     *  分类信息分页查询
     * @param pageNum
     * @param pageSize
     * @param name
     * @param status
     * @return
     */
    R pageList(Integer pageNum, Integer pageSize, String name, String status);


    /**
     * 插入分类信息
     * @param category
     * @return
     */
    R insertCategory(Category category);


    /**
     * 批量删除
     * @param ids
     * @return
     */
    R deleteByList(List<Integer> ids);


    /**
     * 根据id查询分类
     * @param id
     * @return
     */
    R getCategoryById(Long id);


    /**
     * 修改分类信息
     * @return
     */
    R updateCategory(Category category);


    /**
     * 导出分类数据
     * @param response
     */
    void export(HttpServletResponse response);
}

