package com.nn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nn.entity.SysUserRole;


/**
 * 用户和角色关联表(SysUserRole)表服务接口
 *
 * @author 年年
 * @since 2022-12-07 21:40:03
 */
public interface SysUserRoleService extends IService<SysUserRole> {

}

