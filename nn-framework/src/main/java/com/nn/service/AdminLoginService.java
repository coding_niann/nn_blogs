package com.nn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nn.R;
import com.nn.entity.SysUser;

/**
 * 后台用户登录
 * @author 年年 
 * @// TODO: 2022/11/18
 */
public interface AdminLoginService extends IService<SysUser> {

    /**
     * 后台用户登录
     * @param user
     * @return
     */
    R login(SysUser user);


    /**
     * 退出登录
     * @return
     */
    R logout();
}
