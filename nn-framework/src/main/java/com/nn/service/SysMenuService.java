package com.nn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nn.R;
import com.nn.entity.SysMenu;
import com.nn.vo.RoutersVo;

import java.util.List;


/**
 * 菜单权限表(SysMenu)表服务接口
 *
 * @author 年年
 * @since 2022-12-03 23:04:26
 */
public interface SysMenuService extends IService<SysMenu> {

    /**
     * 根据id获取权限信息
     * @param id
     * @return
     */
    List<String> selectPermsByUserId(Long id);


    RoutersVo selectRouterMenuTreeByUserId();

    /**
     * 根据角色id获取菜单权限信息
     * @param id
     * @return
     */
    R getMenuByRoleID(Long id);
}

