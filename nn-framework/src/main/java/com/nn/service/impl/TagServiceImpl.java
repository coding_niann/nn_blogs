package com.nn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nn.R;
import com.nn.domain.dto.TagListDto;
import com.nn.entity.Tag;
import com.nn.enums.AppHttpCodeEnum;
import com.nn.mapper.TagMapper;
import com.nn.service.TagService;
import com.nn.utils.BeanCopyUtils;
import com.nn.vo.ListAllTagVO;
import com.nn.vo.PageVO;
import com.nn.vo.TagPageVO;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * 标签(Tag)表服务实现类
 *
 * @author 年年
 * @since 2022-11-18 19:11:13
 */
@Service("tagService")
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag> implements TagService {


    @Resource
    private TagMapper tagMapper;

    /**
     * 标签列表分页查询
     * @param pageNum
     * @param pageSize
     * @param tagListDto
     * @return
     */
    @Override
    public R<PageVO> pageTagList(Integer pageNum, Integer pageSize, TagListDto tagListDto) {
        //分页查询
        Page<Tag> page = new Page<>();
        LambdaQueryWrapper<Tag> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .like(StringUtils.hasText(tagListDto.getName()),Tag::getName,tagListDto.getName())
                .like(StringUtils.hasText(tagListDto.getRemark()),Tag::getRemark,tagListDto.getRemark());
        page.setCurrent(pageNum);
        page.setSize(pageSize);
        page(page, queryWrapper);
        //封装数据返回
        List<TagPageVO> tagList = BeanCopyUtils.copyBeanList(page.getRecords(), TagPageVO.class);
        PageVO pageVO = new PageVO(tagList,page.getTotal());
        return R.okResult(pageVO);
    }


    /**
     * 添加标签
     * @param tagListDto
     * @return
     */
    @Override
    public R addTag(TagListDto tagListDto) {
        if (!StringUtils.hasText(tagListDto.getName()))
            throw new RuntimeException("请输入标签名");
        Tag tag =  new Tag();
        tag.setName(tagListDto.getName());
        tag.setRemark(tagListDto.getRemark());
        getBaseMapper().insert(tag);
        return R.okResult();
    }


    /**
     * 删除标签
     * @param ids
     * @return
     */
    @Override
    public R deleteList(List<Integer> ids) {
        tagMapper.deleteTagByListID(ids);
        return R.okResult();
    }


    /**
     * 通过id获取标签信息
     * @param id
     * @return
     */
    @Override
    public R getTagInfo(Integer id) {
        if (id==null){
            List<Tag> list = list();
            List<ListAllTagVO> listAllTagVOS = BeanCopyUtils.copyBeanList(list, ListAllTagVO.class);
            return R.okResult(listAllTagVOS);
        }
        LambdaQueryWrapper<Tag> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(Tag::getId,id)
                .eq(Tag::getDelFlag,0);
        Tag tag = tagMapper.selectOne(queryWrapper);
        TagPageVO tagInfo = BeanCopyUtils.copyBean(tag, TagPageVO.class);
        return R.okResult(tagInfo);
    }




    /**
     * 通过id修改标签信息
     * @param tagInfo
     * @return
     */
    @Override
    public R updateTag(TagPageVO tagInfo) {
        if(!StringUtils.hasText(tagInfo.getName()))
            return R.errorResult(AppHttpCodeEnum.TAG_NAME_NOTNULL);
        if(tagInfo.getId()==null)
            return R.errorResult(504,"id不允许为空");
        Tag t = new Tag();
        t.setName(tagInfo.getName());
        t.setRemark(tagInfo.getRemark());
        t.setId(tagInfo.getId());
        updateById(t);
        return R.okResult();
    }
}

