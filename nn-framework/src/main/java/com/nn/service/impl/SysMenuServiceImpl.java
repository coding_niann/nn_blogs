package com.nn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nn.R;
import com.nn.entity.LoginUser;
import com.nn.entity.SysMenu;
import com.nn.mapper.MenuMapper;
import com.nn.service.SysMenuService;
import com.nn.utils.SecurityUtils;
import com.nn.vo.MenuVO;
import com.nn.vo.MenusListVo;
import com.nn.vo.RoutersVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 菜单权限表(SysMenu)表服务实现类
 *
 * @author 年年
 * @since 2022-12-03 23:04:26
 */
@Service("sysMenuService")
public class SysMenuServiceImpl extends ServiceImpl<MenuMapper, SysMenu> implements SysMenuService {


    @Resource
    private SysMenuService menuService;

    /**
     * 根据id获取权限信息
     * @param id
     * @return
     */
    @Override
    public List<String> selectPermsByUserId(Long id) {
        if (id == 1L) {
            LambdaQueryWrapper<SysMenu> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.in(SysMenu::getMenuType,"C","F")
                    .eq(SysMenu::getStatus,0);
            List<SysMenu> list = list(queryWrapper);
            ArrayList<String> resultList = new ArrayList<>();
            for (SysMenu itm : list) {
                resultList.add(itm.getPerms());
            }
            resultList.forEach(System.err::println);
            return resultList;
        }
        return getBaseMapper().selectPermsByUserId(id);

    }


    /**
     * 获取菜单路由
     * @return
     */
    @Override
    public RoutersVo selectRouterMenuTreeByUserId() {
        LoginUser userInfo = SecurityUtils.getLoginUser();
        Long id = userInfo.getUser().getId();
        //判断是否是管理员如果是返回符合要求的menu
        MenuMapper menuMapper = getBaseMapper();
        List<MenusListVo> menus=null;
        if(SecurityUtils.isAdmin()){
           menus= menuMapper.selectAllRouterMenu();
        }else {
             menus= menuMapper.selectRouterMenuById(id);
        }
        //构建tree
        List<MenusListVo> menusTree=builderMenuTree(menus,0L);
        return new RoutersVo(menusTree);
    }


    /**
     * 根据角色id获取菜单信息
     * @param id
     * @return
     */
    //TODO 未完成
    @Override
    public R getMenuByRoleID(Long id) {
        LoginUser userInfo = SecurityUtils.getLoginUser();
        //判断是否是管理员如果是返回符合要求的menu
        MenuMapper menuMapper = getBaseMapper();
        List<MenusListVo> menus=null;
        if(SecurityUtils.isAdmin()){
            menus= menuMapper.selectAllRouterMenu();
        }else {
            menus= menuMapper.selectRouterMenuById(id);
        }
        //构建tree
        List<MenusListVo> menusTree=builderMenuTree(menus,0L);
        MenuVO menuVO = new MenuVO();
        menusTree.forEach(o->menuVO.setMenus(o.getChildren()));
        return R.okResult(menuVO);
    }


    private List<MenusListVo> builderMenuTree(List<MenusListVo> menus, long parentId) {
        List<MenusListVo> menusListVos =menus.stream()
                .filter(menu->menu.getParentId().equals(parentId))
                .map(menu -> menu.setChildren(getChildren(menu, menus)))
                .collect(Collectors.toList());
        return menusListVos;

    }

    private List<MenusListVo> getChildren(MenusListVo menu, List<MenusListVo> menus) {
        List<MenusListVo> childrenList = menus.stream()
                .filter(m -> m.getParentId().equals(menu.getId()))
                .map(m->m.setChildren(getChildren(m,menus)))
                .collect(Collectors.toList());
        return childrenList;
    }


}

