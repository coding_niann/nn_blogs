package com.nn.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nn.entity.SysUserRole;
import com.nn.mapper.SysUserRoleMapper;
import com.nn.service.SysUserRoleService;
import org.springframework.stereotype.Service;

/**
 * 用户和角色关联表(SysUserRole)表服务实现类
 *
 * @author 年年
 * @since 2022-12-07 21:40:03
 */
@Service("sysUserRoleService")
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

}

