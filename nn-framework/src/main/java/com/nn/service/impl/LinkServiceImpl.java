package com.nn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nn.R;
import com.nn.constents.SystemConstants;
import com.nn.entity.Link;
import com.nn.mapper.LinkMapper;
import com.nn.service.LinkService;
import com.nn.utils.BeanCopyUtils;
import com.nn.vo.LinkListVo;
import com.nn.vo.PageVO;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 友链(Link)表服务实现类
 *
 * @author 年年
 * @since 2022-11-12 00:04:12
 */
@Service("linkService")
public class LinkServiceImpl extends ServiceImpl<LinkMapper,Link> implements LinkService {


    /**
     * 显示已通过的友链
     * @return
     */
    @Override
    public R getAllLink() {
        LambdaQueryWrapper<Link> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(Link::getStatus, SystemConstants.LINK_STATUS_NORMAL);
        List<Link> list = list(queryWrapper);
        List<LinkListVo> result = BeanCopyUtils.copyBeanList(list, LinkListVo.class);
        return R.okResult(result);
    }

    /**
     *
     * 友链管理获取友链信息
     * @param pageNum
     * @param pageSize
     * @param name
     * @param status
     * @return
     */
    @Override
    public R getPageList(Integer pageNum, Integer pageSize, String name, String status) {
        LambdaQueryWrapper<Link> queryWrapper  = new LambdaQueryWrapper<>();
        Page<Link> page = new Page<>(pageNum,pageSize);
        queryWrapper
                .like(name!=null,Link::getName,name)
                .eq(status!=null,Link::getStatus,status)
                .eq(Link::getDelFlag,0);
        page(page,queryWrapper);
        List<LinkListVo> resultInfo = BeanCopyUtils.copyBeanList(page.getRecords(), LinkListVo.class);
        PageVO pageVO = new PageVO(resultInfo,page.getTotal());
        return R.okResult(pageVO);
    }


    /**
     * 增加友链
     * @param link
     * @return
     */
    @Override
    public R addLink(Link link) {
        if(!StringUtils.hasText(link.getName()))
            throw new RuntimeException("名称不能为空");
        if(!StringUtils.hasText(link.getLogo()))
            throw new RuntimeException("logo不能为空");
        if(!StringUtils.hasText(link.getAddress()))
            throw new RuntimeException("地址不能为空");
        getBaseMapper().insert(link);
        return R.okResult();
    }


    /**
     * 删除友链
     * @param ids
     * @return
     */
    @Override
    public R delectLink(List<Long> ids) {
        for (int i = 0; i < ids.size(); i++) {
            getBaseMapper().deleteById(list().get(i));
        }
        return R.okResult();
    }

    /**
     * 根据id查询友链
     * @param id
     * @return
     */
    @Override
    public R getListByid(Long id) {
        Link link = getBaseMapper().selectById(id);
        return R.okResult(link);
    }


    /**
     * 修改友链
     * @param link
     * @return
     */
    @Override
    public R updateLink(Link link) {
        if(!StringUtils.hasText(link.getName()))
            throw new RuntimeException("名称不能为空");
        if(!StringUtils.hasText(link.getDescription()))
            throw new RuntimeException("描述不能为空");
        if(!StringUtils.hasText(link.getLogo()))
            throw new RuntimeException("logo不能为空");
        if(!StringUtils.hasText(link.getAddress()))
            throw new RuntimeException("地址信息不能为空");
        getBaseMapper().update(link,null);
        return R.okResult();
    }


    /**
     * 根据id修改审核状态
     * @param id
     * @param status
     * @return
     */
    @Override
    public R updateStatusByID(Link link) {
        getBaseMapper().updateById(link);
        return R.okResult();
    }



}

