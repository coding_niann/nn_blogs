package com.nn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nn.R;
import com.nn.constents.SystemConstants;
import com.nn.entity.Comment;
import com.nn.entity.SysUser;
import com.nn.mapper.CommentMapper;
import com.nn.mapper.SysUserMapper;
import com.nn.service.CommentService;
import com.nn.utils.BeanCopyUtils;
import com.nn.vo.CommentVo;
import com.nn.vo.PageVO;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * 评论表(Comment)表服务实现类
 *
 * @author 年年
 * @since 2022-11-16 00:54:32
 */
@Service("commentService")
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

    @Resource
    private SysUserMapper userMapper;

    /**
     * 获取评论
     *
     * @return
     */
    @Override
    public R getCommentListByarticleId(String type,Long articleId, int pageNum, int pageSize) {
        LambdaQueryWrapper<Comment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(type.equals("0"),Comment::getArticleId,articleId)
                .eq(Comment::getRootId, SystemConstants.RootID)
                .eq(Comment::getType,type);
        //分页查询
        Page<Comment> page = new Page<>(pageNum,pageSize);
        page(page,queryWrapper);
        List<CommentVo> commentVoList =toCommentVoList(page.getRecords());
        // TODO 查询所有根评论的子评论集合children
        for (CommentVo o : commentVoList) {
            List<CommentVo> children = getChildren(o.getId());
            o.setChildren(children);
        }
        return R.okResult(new PageVO(commentVoList,page.getTotal()));
    }



    /**
     * 添加评论
     * @param comment
     * @return
     */
    @Override
    public R addComment(Comment comment) {
        if(!StringUtils.hasText(comment.getContent()))
            throw new RuntimeException("评论内容不能为空");
        if(comment.getContent().contains("你妈")||comment.getContent().contains("尼玛")||comment.getContent().contains("草"))
            throw new RuntimeException("请文明发言，超过三次将进行封号处理");
        save(comment);
        return R.okResult();
    }


    /**
     * todo comment集合转换为commentvo
     * @param list
     * @return
     */
    private List<CommentVo> toCommentVoList(List<Comment> list){
        List<CommentVo> commentVos = BeanCopyUtils.copyBeanList(list, CommentVo.class);
        //遍历commentVos 添加 用户名 toCommentUserName 和 评论人 username
        for (CommentVo vo : commentVos) {
            if(vo.getCreateBy()!=null) {
                long userId = vo.getCreateBy();
                SysUser user = userMapper.selectById(userId);
                //todo 添加username
                if(!Objects.isNull(user)) {
                    String Username = user.getUserName();
                    vo.setUsername(Username);
                    // TODO           添加setToCommentUserName
                    if (vo.getToCommentUserId() != -1) {
                        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
                        queryWrapper.eq(SysUser::getId, vo.getToCommentUserId());
                        SysUser userInfo = userMapper.selectOne(queryWrapper);
                        String nickName = userInfo.getNickName();
                        vo.setToCommentUserName(nickName);
                    }
                }
            }
        }
         return commentVos;
    }


    /**
     * 根据id获取子评论的集合
     * @param id 根评论的id
      * @return List<CommentVo>
     */
    private List<CommentVo> getChildren(Long id){
        LambdaQueryWrapper<Comment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Comment::getRootId,id);
        List<Comment> list = list(queryWrapper);
        return toCommentVoList(list);
    }
}

