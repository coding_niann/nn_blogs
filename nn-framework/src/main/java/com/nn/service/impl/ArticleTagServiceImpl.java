package com.nn.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nn.entity.ArticleTag;
import com.nn.mapper.ArticleTagMapper;
import com.nn.service.ArticleTagService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文章标签关联表(ArticleTag)表服务实现类
 *
 * @author 年年
 * @since 2022-12-12 21:41:16
 */
@Service("articleTagService")
public class ArticleTagServiceImpl extends ServiceImpl<ArticleTagMapper, ArticleTag> implements ArticleTagService {

    @Override
    public List<Long> selectTagIdList(Long id) {
        return getBaseMapper().selectTagIdList(id);
    }
}

