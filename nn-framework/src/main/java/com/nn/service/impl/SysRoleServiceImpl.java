package com.nn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nn.R;
import com.nn.entity.SysRole;
import com.nn.mapper.SysRoleMapper;
import com.nn.service.SysRoleService;
import com.nn.vo.PageVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {



    /**
     * 根据用户id查寻角色信息
     * @param id
     * @return
     */
    @Override
    public List<String> selectRoleKeyByUserId(Long id) {
        //如果是管理员则返回集合种只需要有admin
        if(id == 1L){
            List<String> roleKeyList  = new ArrayList<>();
            roleKeyList.add("admin");
            return roleKeyList;
        }
        return getBaseMapper().selectRoleKeyByUserId(id);

    }


    /**
     * 返回所有角色
     * @return
     */
    @Override
    public R listAll() {
        List<SysRole> list = list();
        return R.okResult(list);
    }


    /**
     * 分页
     * @param pageNum
     * @param pageSize
     * @param roleName
     * @param status
     * @return
     */
    @Override
    public R pageList(Integer pageNum, Integer pageSize, String roleName, String status) {
        Page<SysRole> page = new Page<>(pageNum,pageSize);
        LambdaQueryWrapper<SysRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(roleName!=null,SysRole::getRoleName,roleName);
        queryWrapper.eq(status!=null,SysRole::getStatus,status);
        page(page,queryWrapper);
        PageVO pageInfo = new PageVO(page.getRecords(),page.getTotal());
        return R.okResult(pageInfo);
    }


    /**
     * 修改角色状态
     * @param id
     * @param status
     * @return
     */
    @Override
    public R changeStatus(String id, String status) {
        SysRole role = new SysRole();
        role.setStatus(status);
        role.setId(Long.valueOf(id));
        updateById(role);
        return R.okResult();
    }


    /**
     * 删除角色
     * @param ids
     * @return
     */
    @Override
    public R deleteRoleByID(List<Long> ids) {
        removeByIds(ids);
        return R.okResult();
    }


    /**
     * 根据角色id获取角色信息
     * @param id
     * @return
     */
    @Override
    public R getRoleInfoByID(Long id) {
        SysRole result = getById(id);
        return R.okResult(result);
    }


}
