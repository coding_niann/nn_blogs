package com.nn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nn.R;
import com.nn.domain.dto.AddArticle;
import com.nn.entity.Article;

import java.util.List;

/**
 * 文章表(Article)表服务接口
 *
 * @author Niann
 * @since 2022-11-11 16:13:52
 */

public interface ArticleService extends IService<Article> {

    R hotArticleList();

    /**
     * 文章分页查询
     * @param pageNum
     * @param pageSize
     * @param categoryId
     * @return
     */
    R articleList(Integer pageNum, Integer pageSize, Long categoryId);


    /**
     * 展示文章详细内容
     * @param id
     * @return
     */
    R findIdByArticle(Long id);

    /**
     * 更新文章浏览量
     * @return
     */
    R updateViewCount(Long id);


    /**
     * 文章查询
     * @param pageNum
     * @param pageSize
     * @param title
     * @param summary
     * @return
     */
    R pagetList(Integer pageNum, Integer pageSize, String title, String summary);


    /**
     * 删除文章
     * @param ids
     * @return
     */
    R deleteArticleByid(List<Integer> ids);


    /**
     * 根据id获取文章信息
     * @param id
     * @return
     */
    R getArticleInfoByID(Long id);

    /**
     * 更新文章信息
     * @param article
     * @return
     */
    R updateArticle(Article article);


    /**
     * 添加文章
     * @param article
     * @return
     */
    R add(AddArticle article);
}

