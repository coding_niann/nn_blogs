package com.nn.utils;


import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 对象拷贝工具类
 * @author 年年 
 * &#06;//  TODO: 2022/11/11 19:14
 *
 */
public class BeanCopyUtils {



    @SneakyThrows
    public static <T> T copyBean(Object source, Class<T>  clazz ){
        //创建目标对象
        T result =null;
        result = clazz.newInstance();
        //实现属性拷贝
        BeanUtils.copyProperties(source,result);
        //返回结果
        return result;
    }


    public static <T> List<T> copyBeanList(List<?> list, Class<T> clazz){
       return list.stream()
                .map(o->copyBean(o,clazz))
                .collect(Collectors.toList());
    }

}
