package com.nn.ex;


import com.nn.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * 统一异常处理
 * @author 年年
 * @// TODO: 2022/11/12 03:12
 */
@RestControllerAdvice
@Slf4j
public class ObjExceptionHandler {


    @ExceptionHandler(Exception.class)
    public R ex(Exception e){
        log.error(e.getLocalizedMessage());
        if (e.getMessage().contains("nested exception is org.apache.ibatis.exceptions.PersistenceException:"))
            return R.errorResult(400,"请登录后再操作");
        return R.errorResult(504,e.getLocalizedMessage());
    }

}
