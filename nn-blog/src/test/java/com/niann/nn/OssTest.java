package com.niann.nn;


import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@SpringBootTest
public class OssTest {
    @Value("${oss.ak}")
    private String ak;

    @Value("${oss.sk}")
    private String sk;

    @Value("${oss.name}")
    private String name;

    @Test
    public void test(){
        Configuration cfg =  new Configuration(Region.autoRegion());
        UploadManager uploadManager = new UploadManager(cfg);

        //默认key作为文件名
        String key=null;
        try {

            InputStream file = new FileInputStream("C:\\Users\\gdhlo\\Pictures\\微信图片_20220826194558.jpg");

            Auth auth = Auth.create(ak, sk);
            String upToken = auth.uploadToken(name);
            try {
                Response response = uploadManager.put(file,key,upToken,null, null);
                //解析上传成功的结果
                DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
                System.out.println(putRet.key);
                System.out.println(putRet.hash);
            } catch (QiniuException ex) {
                Response r = ex.response;
                System.err.println(r.toString());
                try {
                    System.err.println(r.bodyString());
                } catch (QiniuException ex2) {
                    //ignore
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
