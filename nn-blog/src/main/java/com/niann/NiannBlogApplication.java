package com.niann;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.CrossOrigin;
@SpringBootApplication()
@MapperScan("com.nn.mapper")
@ComponentScan({"com.nn","com.niann"}) //加这个不然找不到bean
@CrossOrigin
public class NiannBlogApplication  {
    public static void main(String[] args) {
        SpringApplication.run(NiannBlogApplication.class,args);
    }
}
