package com.niann.controller;


import com.nn.R;
import com.nn.service.CategoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 年年 
 * @// TODO: 2022/11/11 20:12
 */
@RestController
@RequestMapping("/category")
class CategoryController {

    @Resource
    private CategoryService  categoryService;

    /**
     * 获取分类
     * @return
     */
    @GetMapping("getCategoryList")
    public R getCategoryList(){
        return categoryService.getCategoryList();
    }
}
