package com.niann.controller;

import com.nn.R;
import com.nn.service.UploadService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Objects;

@RestController
public class UploadController {

    @Resource
    private UploadService uploadService;
    @PostMapping("/upload")
    public R uploadImg(MultipartFile img){
        if (!Objects.isNull(img)){
            return uploadService.uploadImg(img);
        }
        throw  new RuntimeException("只能上传图片文件");
    }
}
