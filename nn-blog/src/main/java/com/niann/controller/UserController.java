package com.niann.controller;


import com.nn.R;
import com.nn.annotation.SystemLog;
import com.nn.entity.SysUser;
import com.nn.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/**
 * @author 年年
 * @// TODO: 2022/11/16 00:22
 */
@RestController()
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    /**
     * 用户注册
     * @param user
     * @return
     */
    @PostMapping("/register")
    public R register(@RequestBody SysUser user){
        System.out.println(user);
        return userService.register(user);
    }


    /**
     * 更新个人信息资料
     * @param user
     * @return
     */
    @PutMapping("/userInfo")
    @SystemLog(businessName = "更新用户信息")
    public R userInfo(@RequestBody SysUser user){
        return userService.updateUserInfo(user);
    }


    /**
     * 查看个人资料
     * @return
     */
    @GetMapping("/userInfo")
    public R SelectUserInfo(){
        return userService.selectUserInfo();
    }
}
