package com.niann.controller;


import com.nn.R;
import com.nn.entity.SysUser;
import com.nn.service.BlogLoginService;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class BlogLonginController {


    @Resource
    private BlogLoginService blogLoginService;


    /**
     * 用户登录接口
     * @param user
     * @return
     */
    @PostMapping("/login")
    public R login(@RequestBody SysUser user){
        if (!StringUtils.hasText(user.getUserName()))
            throw new RuntimeException("请输入用户名和密码");
        return blogLoginService.login(user);
    }

    /**
     * 退出登录
     * @return
     */
    @PostMapping("/logout")
    public R logout(){
        return blogLoginService.logout();
    }
}
