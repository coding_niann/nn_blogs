package com.niann.controller;


import com.nn.R;
import com.nn.service.LinkService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * 友链controller
 * @author 年年 
 * @// TODO: 2022/11/12 00:30
 */
@RestController
@RequestMapping("/link")
public class LinkController {

    @Resource
    private LinkService linkService;


    /**
     * 显示友链
     * @return
     */
    @GetMapping("/getAllLink")
    public R getAllLink(){
        return linkService.getAllLink();
    }
    
}
