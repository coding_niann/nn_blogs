package com.niann.controller;


import com.nn.R;
import com.nn.service.ArticleService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 文章表(Article)表控制层
 *
 * @author makejava
 * @since 2022-11-11 16:11:43
 */
@RestController
@RequestMapping("/article")
public class ArticleController{

    @Resource
    private ArticleService articleService;


    /**
     * 显示文章浏览量前十
     * @return ResponseResult
     */
    @GetMapping("/hotArticleList")
    public R hotArticleList(){
        return articleService.hotArticleList();
    }

    @GetMapping
    public R test(){
        throw new RuntimeException("失败！");
    }

    /**
     * 分页查询
     * @return
     */

    @GetMapping("/articleList")
    public R articleList( Integer pageNum, Integer pageSize, Long categoryId){
        return articleService.articleList(pageNum,pageSize,categoryId);
    }


    /**
     * 查看文章详细
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R findIdByArticle(@PathVariable("id") Long id){
       return  articleService.findIdByArticle(id);
    }


    @PutMapping("/updateViewCount/{id}")
    public R updateViewCount(@PathVariable("id") Long id){
        return articleService.updateViewCount(id);
    }


}

