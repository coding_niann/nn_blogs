package com.niann.controller;


import com.nn.R;
import com.nn.entity.Comment;
import com.nn.service.CommentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 评论控制器
 * @author 年年 
 * @// TODO: 2022/11/16 01:21
 */

@RestController
@RequestMapping("/comment")
public class CommentController {
    @Resource
    private CommentService commentService;


    /**
     * 文章
     * @param articleId
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping("/commentList")
    public R commentList(Long articleId,int pageNum,int pageSize){
        return commentService.getCommentListByarticleId("0",articleId,pageNum,pageSize);
    }


    /**
     * 添加评论
     * @param comment
     * @return
     */
    @PostMapping
    public R addComment(@RequestBody Comment comment){
        return commentService.addComment(comment);
    }


    /**
     * 友链评论展示
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping("linkCommentList")
    public R LinkCommentList(Integer pageNum,Integer pageSize){
        return commentService.getCommentListByarticleId("1",null,pageNum,pageSize);
    }


}
