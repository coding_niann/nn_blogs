package com.nnAdmin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 接收前端信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleStatus {
    private String  roleId ;
    private String status;
}
