package com.nnAdmin.controller;


import com.nn.R;
import com.nn.service.SysRoleService;
import com.nnAdmin.vo.RoleStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色管理
 */
@RestController
@RequestMapping("/system/role")
public class RoleController {


    @Autowired
    SysRoleService roleService;

    /**
     * 返回角色列表
     * @return
     */
    @GetMapping("/listAllRole")
    public R ListAllRole(){
        return roleService.listAll();
    }


    /**
     * 角色分页查询
     * @param pageNum
     * @param pageSize
     * @param roleName
     * @param status
     * @return
     */
    @GetMapping("/list")
    public R listPage(Integer pageNum,Integer pageSize,String roleName,String status){
        return roleService.pageList(pageNum,pageSize,roleName,status);
    }


    /**
     * 设置状态
     * @return
     */
    @PutMapping("/changeStatus")
    public R changeStatus(@RequestBody RoleStatus status){
        String id =status.getRoleId();
        String Status = status.getStatus();
        return roleService.changeStatus(id,Status);
    }


    /**
     * 删除角色
     * @param ids
     * @return
     */
    @DeleteMapping("{ids}")
    public R deleteRole(@PathVariable List<Long> ids){
        return roleService.deleteRoleByID(ids);
    }


    /**
     * 修改角色时获取角色信息
     */
    @RequestMapping(value = "{id}" ,method = RequestMethod.GET)
    public R getRoleByID(@PathVariable Long id){
        return roleService.getRoleInfoByID(id);
    }
}
