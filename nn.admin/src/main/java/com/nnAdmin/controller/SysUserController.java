package com.nnAdmin.controller;


import com.nn.R;
import com.nn.entity.SysUser;
import com.nn.service.UserService;
import com.nn.vo.UserVO;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


/**
 * 系统管理控制类
 * @author 年年 
 * @// TODO: 2022/12/6
 */
@RestController
@RequestMapping("/system/user")
public class SysUserController {

    @Resource
    private UserService service;

    /**
     * 用户信息查询
     * @param pageNum
     * @param pageSize
     * @return R
     */
    @GetMapping("/list")
    public R listPage(Integer pageNum, Integer pageSize, SysUser user){
        String userName =user.getUserName();
        String phoneNumber = user.getPhonenumber();
        String status = user.getStatus();
        return service.pageList(pageNum,pageSize,userName,phoneNumber,status);
    }


    /**
     * 设置停用和启用
     * @param
     * @return
     */
    @PutMapping("/changeStatus")
    public R updateStatus(@RequestBody UserVO user){
        return service.updateStatus(user.getUserId(),user.getStatus());
    }


    /**
     * 修改时获取用户信息
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public R getUserInfo(@PathVariable Long id){
        return service.selectUserInfoById(id);
    }


    /**
     * 更新用户信息
     * @param user
     * @return
     */
    @PutMapping
    public R updateUser(@RequestBody SysUser user){
        return  service.updateUserInfo(user);
    }


    /**
     * 新增用户
     * @return
     */
    @PostMapping
    public R InsertUser(@RequestBody SysUser user){
        return service.addUser(user);
    }

    /**
     * 删除用户
     * @return
     */
    @DeleteMapping("{ids}")
    public R deleteUser(@PathVariable List<Long> ids){
        return service.deleteUser(ids);
    }



}
