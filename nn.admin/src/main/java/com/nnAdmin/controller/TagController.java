package com.nnAdmin.controller;


import com.nn.R;
import com.nn.domain.dto.TagListDto;
import com.nn.service.TagService;
import com.nn.vo.PageVO;
import com.nn.vo.TagPageVO;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 年年 
 * @// TODO: 2022/11/18 19:24
 */

@RestController()
@RequestMapping("/content/tag")
public class TagController {

    @Resource
    private TagService tagService;

    /**
     * 查询标签列表
     * @param pageNum
     * @param pageSize
     * @param tagListDto
     * @return
     */
    @GetMapping("/list")
    public R<PageVO> list(Integer pageNum, Integer pageSize, TagListDto tagListDto){
        return tagService.pageTagList(pageNum,pageSize,tagListDto);
    }


    /**
     * 添加标签
     * @param tagListDto
     * @return
     */
    @PostMapping
    public R addTag(@RequestBody TagListDto tagListDto){
        return tagService.addTag(tagListDto);
    }


    /**
     * 删除标签
     * @param ids
     * @return
     */
    @DeleteMapping("{ids}")
    public R deleteTage(@PathVariable List<Integer> ids){
        return tagService.deleteList(ids);
    }


    /**
     * 通过id获取标签信息
     * @param id
     * @return
     */

    @GetMapping({"/{id}"})
    public R getTagInfo(@PathVariable Integer id){
        return tagService.getTagInfo(id);
    }


    /**
     * 获取所有标签
     * @param
     * @return
     */
    @GetMapping("/listAllTag")
    public R getTagAll(){
        return tagService.getTagInfo(null);
    }


    /**
     * 通过id修改标签信息
     * @param tagInfo
     * @return
     */
    @PutMapping
    public R updateTag(@RequestBody TagPageVO tagInfo){
        return tagService.updateTag(tagInfo);
    }


}
