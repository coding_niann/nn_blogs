package com.nnAdmin.controller;


import com.nn.R;
import com.nn.entity.Link;
import com.nn.service.LinkService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/content/link")
public class ContentController {
//    http://localhost:81/dev-api/content/link/list?pageNum=1&pageSize=10
    @Resource
    private LinkService linkService;


    /**
     *  友链管理查询
     * @param pageNum
     * @param pageSize
     * @param name
     * @param status
     * @return
     */
    @GetMapping("/list")
    public R listPage(Integer pageNum,Integer pageSize,String name,String status){
        return linkService.getPageList(pageNum,pageSize,name,status);
    }


    /**
     * 新增友链
     * @param link
     * @return
     */
    @PostMapping
    public R addLink(@RequestBody Link link){
        return linkService.addLink(link);
    }

    /**
     * 删除友链
     * @param ids
     * @return
     */
    @DeleteMapping("{ids}")
    public R delectList(@PathVariable List<Long> ids){
        return linkService.delectLink(ids);
    }


    /**
     * 根据id查询友链
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public R getListById(@PathVariable Long id){
        return linkService.getListByid(id);
    }

    /**
     * 修改友链信息
     * @param link
     * @return
     */
    @PutMapping
    public R updateLink(@RequestBody Link link){
        return linkService.updateLink(link);
    }


    /**
     * 根据id修改审核状态
     * @param link
     * @return
     */
    @PutMapping("/changeLinkStatus")
    public R changeLinkStatus(@RequestBody Link link){
        return linkService.updateStatusByID(link);
    }
}
