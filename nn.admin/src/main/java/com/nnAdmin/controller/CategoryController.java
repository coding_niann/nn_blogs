package com.nnAdmin.controller;


import com.nn.R;
import com.nn.entity.Category;
import com.nn.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author 年年
 * 文章分类接口
 * @// TODO: 2022/12/6
 */

@RestController
@RequestMapping("/content/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;


    /**
     * 分类管理分页查询
     * @param pageNum
     * @param pageSize
     * @param name
     * @param status
     * @return
     */
    @GetMapping("/list")
    public R pageList(Integer pageNum,Integer pageSize,String name,String status){
        return categoryService.pageList(pageNum,pageSize,name,status);
    }


    /**
     * 查询所有分类
     */
    @GetMapping("/listAllCategory")
    public R getListAllCategory(){
       return categoryService.getListAllCategory();
    }


    /**
     * 新增分类
     * @param category
     * @return
     */
    @PostMapping
    public R addCategory(@RequestBody Category category){
        return categoryService.insertCategory(category);
    }

    /**
     * 删除分类
     * @param ids
     * @return
     */
    @DeleteMapping("/{ids}")
    public R deleteCategory(@PathVariable List<Integer> ids){
        return categoryService.deleteByList(ids);
    }


    /**
     * 根据id查询分类信息
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public R getCategoryInfoById(@PathVariable Long id){
        return categoryService.getCategoryById(id);
    }


    @PutMapping
    public R updateCategory(@RequestBody Category category ){
        return categoryService.updateCategory(category);
    }


    /**
     * 导出excel
     * @return
     */
    @GetMapping("/export")
    public void export(HttpServletResponse response){
        categoryService.export(response);
    }
}
