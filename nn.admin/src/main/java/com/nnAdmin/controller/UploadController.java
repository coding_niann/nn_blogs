package com.nnAdmin.controller;


import com.nn.R;
import com.nn.service.UploadService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;


/**
 * 上传图片
 * @author 年年
 * @// TODO: 2022/12/12
 */
@RestController
@RequestMapping("/upload")
public class UploadController {

    @Resource
    private UploadService uploadService;

    /**
     * 存储文件缩略图
     * @param multipartFile
     * @return
     * @throws IOException
     */
    @PostMapping
    public R uploadImg(@RequestParam("img")MultipartFile multipartFile) throws IOException {
        return uploadService.uploadImg(multipartFile);
    }
}
