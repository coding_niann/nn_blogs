package com.nnAdmin.controller;


import com.nn.R;
import com.nn.service.SysMenuService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 菜单
 */
@RestController
@RequestMapping("/system/menu")
public class MenuController {


    @Resource
    private SysMenuService menuService;

    /**
     * 修改角色时获取信息
     * @param id
     * @return
     */
    @GetMapping("/roleMenuTreeselect/{id}")
    public R updateRole(@PathVariable Long id){
        return menuService.getMenuByRoleID(id);
    }

}
