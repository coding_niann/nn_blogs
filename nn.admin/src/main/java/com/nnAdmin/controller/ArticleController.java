package com.nnAdmin.controller;


import com.nn.R;
import com.nn.domain.dto.AddArticle;
import com.nn.entity.Article;
import com.nn.service.ArticleService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


/**
 * 文章控制层
 * @author 年年 
 * @// TODO: 2022/12/6
 */
@RestController
@RequestMapping("/content/article")
public class ArticleController {


    @Resource
    private ArticleService articleService;

    /**
     * 文章管理查询查询
     * @param pageNum
     * @param pageSize
     * @param title
     * @param summary
     * @return
     */
    @GetMapping("/list")
    public R pageList(Integer pageNum,Integer pageSize,String title,String summary){
        return articleService.pagetList(pageNum,pageSize,title,summary);
    }


    /**
     * 删除文章
     * @param ids
     * @return
     */
    @DeleteMapping("{ids}")
    public R deleteArticle(@PathVariable List<Integer> ids){
        return articleService.deleteArticleByid(ids);
    }


    /**
     * 根据id获取文章信息
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public R getArticleInfo(@PathVariable Long id){
        return articleService.getArticleInfoByID(id);
    }

    /**
     * 更新文章
     * @param article
     * @return
     */
    @PutMapping
    public R updateByID(@RequestBody Article article){
        return articleService.updateArticle(article);
    }

    /**
     * 写博文
     * @param article
     * @return
     */
    @PostMapping
    public R add(@RequestBody AddArticle article){
        return articleService.add(article);
    }
}
