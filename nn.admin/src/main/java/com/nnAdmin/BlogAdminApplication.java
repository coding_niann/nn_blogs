package com.nnAdmin;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 * @author 年年 
 * @// TODO: 2022/11/18 19:24
 */
@SpringBootApplication
@ComponentScan({"com.nn","com.nnAdmin"}) //加这个不然找不到bean
@MapperScan("com.nn.mapper")
public class BlogAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(BlogAdminApplication.class,args);
    }
}
